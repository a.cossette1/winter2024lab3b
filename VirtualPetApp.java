import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Turtle[] bale = new Turtle[4];
		
		for(int i = 0; i < bale.length; i++){
			bale[i] = new Turtle();
			
			System.out.println("Is your turtle a land or sea turtle?");
			bale[i].habitat = reader.nextLine();
			
			System.out.println("How much does your turtle weigh?");
			bale[i].weight = Double.parseDouble(reader.nextLine());
			
			System.out.println("Is your turtle friends with Master Oogway? True or False");
			bale[i].isFriendsWithOogway = Boolean.parseBoolean(reader.nextLine());
		}
		System.out.println(bale[bale.length - 1].habitat + " " + bale[bale.length - 1].weight + " " + bale[bale.length - 1].isFriendsWithOogway);
		bale[bale.length - 1].eatSeaweed();
		bale[bale.length - 1].sayHiToOogway();
	}
}