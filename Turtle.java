public class Turtle {
	public String habitat;
	public double weight;
	public boolean isFriendsWithOogway;
	
	public void eatSeaweed(){
		System.out.print("Your turle gained 6 pounds after eating 5 pieces of seaweed! It now weighs: " + (this.weight + 6));
	}
	
	public void sayHiToOogway() {
		if(this.isFriendsWithOogway == true) {
			System.out.println("Master Oogway says Hello Friend!");
		} else {
			System.out.println("Master Oogway is upset that you said you were not friends and he NEVER wants to talk again.");
		}
	}
}
